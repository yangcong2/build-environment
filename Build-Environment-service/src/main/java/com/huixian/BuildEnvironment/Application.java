/*
 * Copyright (c) 2018. Polegek
 */

package com.huixian.BuildEnvironment;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

/**
 * 项目启动类.
 *
 * @author : polegek
 * @date : Created in 2018/10/17 2:46
 */
@SpringBootApplication
@Slf4j
//@EnableDubbo
@MapperScan(
        basePackages = {"com.huixian.*.dmr.*.mapper"}
)
public class Application {
    public static void main(String[] args) {
        System.out.println("                                                     ");
        System.out.println("                       _oo0oo_                       ");
        System.out.println("                      o8888888o                      ");
        System.out.println("                      88\" . \"88                    ");
        System.out.println("                      (| -_- |)                      ");
        System.out.println("                      0\\  =  /0                     ");
        System.out.println("                    ___/‘---’\\___                   ");
        System.out.println("                  .' \\|       |/ '.                 ");
        System.out.println("                 / \\\\|||  :  |||// \\              ");
        System.out.println("                / _||||| -卍-|||||_ \\                ");
        System.out.println("               |   | \\\\\\  -  /// |   |            ");
        System.out.println("               | \\_|  ''\\---/''  |_/ |             ");
        System.out.println("               \\  .-\\__  '-'  ___/-. /             ");
        System.out.println("             ___'. .'  /--.--\\  '. .'___            ");
        System.out.println("          .\"\" ‘<  ‘.___\\_<|>_/___.’ >’ \"\".      ");
        System.out.println("         | | :  ‘- \\‘.;‘\\ _ /’;.’/ - ’ : | |       ");
        System.out.println("         \\  \\ ‘_.   \\_ __\\ /__ _/   .-’ /  /     ");
        System.out.println("     =====‘-.____‘.___ \\_____/___.-’___.-’=====     ");
        System.out.println("                       ‘=---=’                       ");
        System.out.println("                                                     ");
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    @Bean
    public FilterRegistrationBean testFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
//        registration.setFilter(new HuiXianRequestFilter());
        registration.addUrlPatterns(new String[]{"/*"});
        registration.setName("huiXianRequestFilter");
        registration.setOrder(-100);
        return registration;
    }
}
