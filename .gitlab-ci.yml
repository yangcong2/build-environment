
#[doc]:       https://docs.gitlab.com/ce/ci/yaml/
#             https://docs.gitlab.com/ce/ci/variables/README.html
#[validate]:  http://gitlab.julanling.com/ci/lint

# version: 2019-02-11-SNAPSHOT

stages:
  - check
  - build-dubbo
  - dev
  - test
  - pre-prd
  - notify

variables:
  LANG: "en_US.UTF-8"
  FAIL_NOTIFY: "fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171"   # 失败告警机器人
  DEPLOY_NOTIFY: "fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171" # 部署通知机器人
  DEPLOY_TIPS: "发布注意事项（SLB切流等）" # 生成发布信息时，自动填写的发布信息


#--- 全局优先脚本
before_script:
  # 计算当前打包环境
  - "_PROFILE=$(bash /var/www/gitlab-ci/java-business/mvn__get_business_profile.sh)"
  - "SNAPSHOT_DEPLOY_PARAM=$(bash /var/www/gitlab-ci/java-business/configuration.sh SNAPSHOT_DEPLOY_PARAM)"
  - "RELEASE_DEPLOY_PARAM=$(bash /var/www/gitlab-ci/java-business/configuration.sh RELEASE_DEPLOY_PARAM)"

#--- [check] 校验是否包含最新master
check_contain_latest_master:
  script:
    # 检查是否包含最新master，若未包含，钉钉失败通知（文案可编辑）
    - "bash /var/www/gitlab-ci/java-business/success_if_include_latest_master.sh
      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 未包含最新master节点"
  when: always
  allow_failure: false
  stage: check

#--- [check] 检查Dubbo服务包中的规范
check_dubbo_standard:
  script:
    # 检测Dubbo相关文件夹内的命名规范
    - "bash /var/www/gitlab-ci/java-business/fail_if_dubbo_file_legal.sh
      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} Dubbo命名不规范"
    # 尝试打包，检测是否存在打包异常情况
    # 编译失败，钉钉告警（文案可编辑）
    - "bash /var/www/gitlab-ci/java-business/fail_if_file_or_dir_modified.sh ./pom.xml ./${CI_PROJECT_NAME}-dubbo/*
      || mvn -pl ${CI_PROJECT_NAME}-dubbo -am clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true verify
      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} Dubbo包校验失败"
  when: always
  tags:
    - Java
  allow_failure: false
  stage: check


#--- [build-dubbo] 检测Dubbo包是否需要打包
deploy_dubbo_dev:
  script:
    # 检测到 dubbo-api的源码 或 pom.xml变更 后 才触发编译 => install,deploy
    # 编译的同时，会编译与其相关联的包，并发布到仓库
    # 编译或发布失败，钉钉告警（文案可编辑）
    - "bash /var/www/gitlab-ci/java-business/fail_if_file_or_dir_modified.sh ./pom.xml ./${CI_PROJECT_NAME}-dubbo/*
      || mvn -pl ${CI_PROJECT_NAME}-dubbo -am clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true deploy ${SNAPSHOT_DEPLOY_PARAM}
      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} Dubbo打包失败，原因未知"
  when: on_success
  tags:
    - Java
  allow_failure: true
  except:
    refs:
      - master
      - pre
  stage: build-dubbo

#--- [build-dubbo] 检测Dubbo包并按需打包，发送钉钉通知，脚本逻辑同
deploy_dubbo_pre_prd:
  script:
    # 检测到dubbo-api的源码或pom.xml变更后 才触发编译 => install,deploy
    # 编译的同时，会编译与其相关联的包，并发布到仓库
    - "bash /var/www/gitlab-ci/java-business/fail_if_file_or_dir_modified.sh ./pom.xml ./${CI_PROJECT_NAME}-dubbo/*
        || mvn -pl ${CI_PROJECT_NAME}-dubbo -am clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true deploy ${RELEASE_DEPLOY_PARAM}
        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} Dubbo打包失败，原因未知"
    # 编译发布成功后，发送钉钉通知
    - "bash /var/www/gitlab-ci/java-business/fail_if_file_or_dir_modified.sh ./pom.xml ./${CI_PROJECT_NAME}-dubbo/*
        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_dubbo_release.sh ${DEPLOY_NOTIFY}"
  when: on_success
  tags:
    - Java
  allow_failure: true
  only:
    refs:
      - master
      - pre
  stage: build-dubbo


#--- [dev] maven打应用包
build_dev:
  script:
    # 尝试编译执行包，若失败则告警（文案可编辑）
    - "mvn -f ./${CI_PROJECT_NAME}-service/pom.xml clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true verify
        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 编译失败"
    # 自动编译成功后, 如果git提交msg中包含了 `[dev]` 再继续执行deploy
    - "bash /var/www/gitlab-ci/java-business/fail_if_commit_msg_contain.sh '[dev]'
        || bash /var/www/gitlab-ci/java-business/deploy_to_dev.sh"
  when: on_success
  tags:
    - Java
    - dev
  allow_failure: false
  stage: dev

#--- [dev] 测试环境部署
deploy_dev:
  script:
    # 尝试编译执行包，若失败则告警（文案可编辑）
    - "mvn -f ./${CI_PROJECT_NAME}-service/pom.xml clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true verify
        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 编译失败"
    # 执行部署
    - "bash /var/www/gitlab-ci/java-business/deploy_to_dev.sh"
  when: manual
  tags:
    - Java
    - dev
  allow_failure: true
  stage: dev


##--- [test] maven打应用包 测试环境打包
#build_test:
#  script:
#    # 尝试编译执行包，若失败则告警（文案可编辑，所有失败其实会在前置被拦截）
#    - "mvn -f ./${CI_PROJECT_NAME}-service/pom.xml clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true verify
#        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 编译失败"
#    # 自动编译成功后，将jar包拷贝到指定目录，写下jar包名称
#    - "bash /var/www/gitlab-ci/java-business/deploy_to_test.sh
#      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 请求部署失败"
#    # 发送申请测试的钉钉通知
#    - "bash /var/www/gitlab-ci/java-business/notify_test_deploy.sh ${DEPLOY_NOTIFY}"
#  when: on_success
#  tags:
#    - Java
#    - test
#  allow_failure: false
#  only:
#    refs:
#      - test
#  stage: test
#
##--- [test] maven打应用包 测试环境（手工）
#build_test_manual:
#  script:
#    # 尝试编译执行包，若失败则告警（文案可编辑，所有失败其实会在前置被拦截）
#    - "mvn -f ./${CI_PROJECT_NAME}-service/pom.xml clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true verify
#        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 编译失败"
#    # 自动编译成功后，将jar包拷贝到指定目录，写下jar包名称，并发送申请测试环境发布通知
#    - "bash /var/www/gitlab-ci/java-business/deploy_to_test.sh
#      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 请求部署失败"
#    # 发送申请测试的钉钉通知
#    - "bash /var/www/gitlab-ci/java-business/notify_test_deploy.sh ${DEPLOY_NOTIFY}"
#  when: manual
#  tags:
#    - Java
#    - test
#  allow_failure: true
#  only:
#    refs:
#      - pre
#      - master
#  stage: test
#
#
##--- [pre-prd] maven打应用包 待预发布
#build_pre:
#  script:
#    # 尝试编译执行包，若失败则告警（文案可编辑）
#    - "mvn -f ./${CI_PROJECT_NAME}-service/pom.xml clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true verify
#        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 编译失败"
#    # 部署到预发布指定目录并钉钉通知
#    - "bash /var/www/gitlab-ci/java-business/deploy_to_pre.sh
#      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 请求部署失败"
#    # 发送上预发布邮件的钉钉内容
#    - "bash /var/www/gitlab-ci/java-business/notify_pre_deploy.sh ${DEPLOY_NOTIFY}"
#  when: on_success
#  tags:
#    - Java
#    - test
#  allow_failure: false
#  only:
#    refs:
#      - pre
#  stage: pre-prd
#
##--- [pre-prd] maven打应用包 待预发布（手工）
#build_pre_manual:
#  script:
#    # 尝试编译执行包，若失败则告警（文案可编辑）
#    - "mvn -f ./${CI_PROJECT_NAME}-service/pom.xml clean -Dbranch_name=${CI_COMMIT_REF_NAME} -P $_PROFILE -Dmaven.test.skip=true verify
#        || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 编译失败"
#    # 部署到预发布指定目录并钉钉通知
#    - "bash /var/www/gitlab-ci/java-business/deploy_to_pre.sh
#      || bash /var/www/gitlab-ci/java-business/notify_dingtalk_when_failed.sh ${FAIL_NOTIFY} 请求部署失败"
#    # 发送上预发布邮件的钉钉内容
#    - "bash /var/www/gitlab-ci/java-business/notify_pre_deploy.sh ${DEPLOY_NOTIFY}"
#  when: manual
#  tags:
#    - Java
#    - test
#  allow_failure: false
#  only:
#    refs:
#      - master
#  stage: pre-prd
#
#
##--- [notify] 将要发布到pre的信息进行整合
#notify_pre:
#  script:
#    # 发送上预发布通知
#    - "bash /var/www/gitlab-ci/java-business/notify_pre_deploy.sh ${DEPLOY_NOTIFY} "
#  when: manual
#  tags:
#    - Java
#    - test
#  allow_failure: false
#  only:
#    refs:
#      - pre
#      - master
#  stage: notify
#
##--- [notify] 将要发布到prd的信息进行整合
#notify_prd:
#  script:
#    # 发送上线通知
#    - "bash /var/www/gitlab-ci/java-business/notify_prd_deploy.sh ${DEPLOY_NOTIFY} ${DEPLOY_TIPS}"
#  when: manual
#  tags:
#    - Java
#    - test
#  allow_failure: false
#  only:
#    refs:
#      - pre
#      - master
#  stage: notify
